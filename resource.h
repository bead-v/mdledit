#ifndef RESOURCE_H_INCLUDED
#define RESOURCE_H_INCLUDED

//The icon
#define IDI_ICON            1
#define IDI_ICON2           2
#define IDI_DLG_ICON        3
#define ID_CUR_DRAG         4

//The menu
#define IDM_MAIN            100
#define IDM_FILE_NEW        101
#define IDM_FILE_SAVE       102
#define IDM_FILE_SAVEAS     103
#define IDM_FILE_EXIT       104
#define IDM_ZOOM_IN         105
#define IDM_ZOOM_OUT        106
#define IDM_MDLEDIT         107
#define IDM_POINT_NEW       108
#define IDM_POINT_DEL_LAST  109
#define IDM_NORTH           110
#define IDM_SOUTH           111
#define IDM_WEST            112
#define IDM_EAST            113
#define IDM_FILE_EXPORT     114
#define IDM_FILE_OPEN       115
#define IDM_DRAW_H          116

#define IDM_MDL_OPEN        117
#define IDM_ASCII_OPEN      118
#define IDM_BIN_SAVE        119
#define IDM_MDX_SAVE        120
#define IDM_WOK_SAVE        121
#define IDM_ASCII_SAVE      122
//#define IDM_LINK_HEAD       123
#define IDM_SETTINGS        124
#define IDM_EDITOR_DLG      125
#define IDM_TREE_SORT_LIN   126
#define IDM_TREE_SORT_HIE   127
#define IDM_EDIT_TEXTURES   128
#define IDM_VIEW_HEX        129
#define IDM_HELP            130
#define IDM_MASS_TO_ASCII   131
#define IDM_MASS_TO_BIN     132
//#define IDM_PLATFORM_PC     133
//#define IDM_PLATFORM_XBOX   134
#define IDM_MASS_ANALYZE    135
#define IDM_BIN_COMPARE     136
#define IDM_SHOW_DIFF       137
#define IDM_SHOW_GROUP      138
#define IDM_SHOW_DATASTRUCT 139
#define IDM_SHOW_REPORT     140
#define IDM_AUTO_SCROLL     141

//GUI controls
#define IDC_MAIN_EDIT    201
#define IDC_TABS         202

#define IDC_LBL_CHAR     203
#define IDC_LBL_INT      204
#define IDC_LBL_UINT     205
#define IDC_LBL_FLOAT    206
#define IDC_EDIT_CHAR    207
#define IDC_EDIT_INT     208
#define IDC_EDIT_UINT    209
#define IDC_EDIT_FLOAT   210
#define IDC_EDIT_DISPLAY 211
#define IDC_TREEVIEW     212
#define IDD_EDITOR_DLG   213
#define IDC_TEXTURE_LISTVIEW1  214
#define IDC_TEXTURE_LISTVIEW2  215
#define IDC_TEXTURE_LISTVIEW3  216
#define IDC_TEXTURE_LISTVIEW4  217
#define IDC_BTN_GAME     218
#define IDC_BTN_PLATFORM 219
#define IDC_BTN_NECK     220

#define IDC_SBS_SBH      230
#define IDC_SBS_SBV      231
#define IDC_SBS_SBC      232

#define IDC_STATUSBAR               251
#define IDC_STATUSBAR_PROGRESS      252
#define IDC_STATUSBAR_PROGRESSMASS  253

#define IDPM_TV_FOLD                301
#define IDPM_TV_EXPAND              302
#define IDPM_TV_FOLD_CHILDREN       303
#define IDPM_TV_EXPAND_CHILDREN     304
#define IDPM_VIEW_ASCII             305
#define IDPM_OPEN_GEO_VIEWER        306
#define IDPM_OPEN_EDITOR            307
#define IDPM_SCROLL                 308
#define IDPM_TV_FOLD_ALL            309
#define IDPM_TV_EXPAND_ALL          310

#define IDP_DISPLAY_UPDATE      350

#define DLG_SETTINGS            401
#define DLG_ID_AREA_WEIGHT      402
#define DLG_ID_ANGLE_WEIGHT     403
#define DLG_ID_STATIC           404
#define DLG_PROGRESS            405
#define DLG_EDIT_TEXTURES       406
#define DLG_ID_CALC_SMOOTHING   407
#define DLG_ABOUT               408
#define DLG_ID_SKIN_TRIMESH     409
#define DLG_ID_SABER_TRIMESH    410
#define DLG_ID_DO_ANIMATIONS    411
#define DLG_ID_BEZIER_LINEAR    412
#define DLG_ID_EXPORT_WOK       413
#define DLG_PROGRESSMASS        414
#define DLG_ID_DOT_ASCII        415
#define DLG_ID_SAVE_REPORT      416
#define DLG_ID_MIN_VERT         417
#define DLG_ID_MIN_VERT2        418
#define DLG_ID_CREASE_ANGLE     419
#define DLG_ID_CREASE_ANGLE_DEG 420
#define DLG_ID_WOK_COORDS       421


#define IDDB_EDIT               500
#define IDDB_SAVE               501

#define IDDB_DIALOG_TYPE_1      1200

#endif // RESOURCE_H_INCLUDED
